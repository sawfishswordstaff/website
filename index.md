---
title: usernamesystems
---

# usernamesystems@website

```
____|____
    |
    |
   / \
  /   \
```

### Welcome to my network.

> Game dev, web dev, and a free (as in freedom) software dev.

- [Gitlab](https://gitlab.com/sawfishswordstaff)
- [Youtube](https://www.youtube.com/channel/UCQN_v9YmP7y0EJE4B9czshg/)

### Creations
- [Stardew Valley Mods](https://github.com/JessebotX/StardewValleyMods)

### Directories

Here's what on this website...

- [blog/](/blog)
    - Blog... if I ever blog
- [dev/](/dev)
    - Full of cool projects I've made and/or contributed
- [misc/](/misc)
    - Miscellaneous pages
